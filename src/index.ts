import readline from 'readline';

interface IForecastV1SelectBody {
  originGeoId?: string;
  origin3DZGeoId?: string;
  originKMAGeoId?: string;
  originXMAGeoId?: string;
  originRegionGeoId?: string;
  originCountryGeoId?: string;
  destinationGeoId?: string;
  destination3DZGeoId?: string;
  destinationKMAGeoId?: string;
  destinationXMAGeoId?: string;
  destinationRegionGeoId?: string;
  destinationCountryGeoId?: string;
  equipmentCategory: string;
  tripTypeVal: number;
  daysBackVal: number;
  forecastDensity: string;
  forecastUnitsVal: number;
  rateFormatVal: string;
  rateTypeVal: number;
  today: Date;
}

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms, null));
}

async function getInput(): Promise<string> {
  const processor = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  let input = '';
  processor.question(
    'Input full JSON object from log statement: ',
    (it: string) => {
      input = it;
      processor.close();
    },
  );

  while (input == '') {
    await sleep(10);
  }

  return input;
}

function fail(message: string) {
  console.log('\n');
  console.error(message, '\n');
  process.exit(0);
}

function extractParameterizedSQL(message: string) {
  const start = message.indexOf('[');
  const end = message.lastIndexOf(']');
  if (start < 0 || end < 0) {
    fail(`Could not parse SQL portion of message: ${message}`);
  }

  return message.substring(start + 1, end).trim();
}

function extractSQLParameters(message: string) {
  const search = 'Payload:';
  const start = message.indexOf(search);
  if (start < 0) {
    fail(`Could not find parameters (Payload) in message: ${message}`);
  }
  const forecastV1SelectBody = message.substring(start + search.length);

  let body;
  try {
    body = JSON.parse(forecastV1SelectBody);
  } catch {
    fail(`Unable to process this string as JSON: ${body}`);
  }

  return body as IForecastV1SelectBody;
}

function parseInput(input: string): [sql: string, obj: IForecastV1SelectBody] {
  let json = null;
  try {
    json = JSON.parse(input);
  } catch {
    fail('Invalid JSON object');
  }

  const message = json['message'];
  if (!message) {
    fail('Expected "message" in JSON paylaod');
  }

  const sql = extractParameterizedSQL(message);
  const selectBody = extractSQLParameters(message);

  return [sql, selectBody];
}

const padWithLeadingZero = (num: number): string =>
  num < 10 ? '0' + num : '' + num;

function formatParameterizedSQL(
  selectQueryString: string,
  payload: IForecastV1SelectBody,
): string {
  let result: string = selectQueryString;
  for (const [key, value] of Object.entries(payload)) {
    let substitutionValue = value;

    if (key === 'today') {
      const today = new Date(value);
      const year = today.getFullYear();
      const month = padWithLeadingZero(today.getMonth() + 1);
      const day = padWithLeadingZero(today.getDate());
      substitutionValue = `${year}-${month}-${day}`;
    }

    const shouldWrapInQuotes = typeof value != 'number';
    if (shouldWrapInQuotes) {
      substitutionValue = `'${substitutionValue}'`;
    }

    result = result.replace(new RegExp(`:${key}`, 'g'), substitutionValue);
  }

  return result + ';';
}

async function main() {
  const input = await getInput();
  const [sql, forecastV1SelectBody] = parseInput(input);
  const formatted = formatParameterizedSQL(sql, forecastV1SelectBody);
  console.log('\n\nResults of formatting:');
  console.log(formatted);
}

main();
